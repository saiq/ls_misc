//
//  FilterViewController.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

open class FilterViewController: UICollectionViewController {
    
    // MARK: Public Properties
    
    public var filterData: FilterData! {
        didSet {
            self.collectionView?.reloadData()
        }
    }
    public var columnNumber: Int = 4 {
        didSet {
            if self.columnNumber < 1 {
                self.columnNumber = oldValue
                return
            }
            
            self.updateLayout()
        }
    }
    public var filterItemCell: ReusableView<FilterItemCell>! = ReusableView<FilterItemCell>("FilterItemCell")
    public weak var delegate: FilterViewControllerDelegate?
    
    // MARK: Lifecycle
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView!.allowsSelection = false
        
        self.updateLayout()
    }
    
    // MARK: - Update
    
    fileprivate func updateLayout() {
        let count = CGFloat(self.columnNumber)
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize.width = (self.collectionView!.frame.size.width
            - layout.sectionInset.left
            - layout.sectionInset.right
            - layout.minimumInteritemSpacing * (count - 1))
            / count
    }
}

// MARK: - Instantiate

extension FilterViewController {
    public static func instantiate(filterData: FilterData, columnNumber: Int = 4) -> FilterViewController {
        let viewController = self.storyboard(name: "Main").instantiateViewController(withIdentifier: "Filter") as! FilterViewController
        viewController.filterData = filterData
        viewController.columnNumber = columnNumber
        return viewController
    }
}

// MARK: - UICollectionViewDataSource

extension FilterViewController {
    open override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    open override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filterData.items.count
    }
    
    open override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(self.filterItemCell, for: indexPath)
        cell.delegate = self
        cell.button.setTitle(self.filterData.items[indexPath.item].text, for: .normal)
        cell.button.isSelected = self.filterData.isSelected(index: indexPath.item)
        return cell
    }
}

// MARK: - FilterViewControllerDelegate

public protocol FilterViewControllerDelegate: class {
    func filterViewController(_ viewController: FilterViewController, didSelectItemAt index: Int)
    func filterViewController(_ viewController: FilterViewController, didDeselectItemAt index: Int)
    func filterViewController(_ viewController: FilterViewController, shouldSelectItemAt index: Int) -> Bool
    func filterViewController(_ viewController: FilterViewController, shouldDeselectItemAt index: Int) -> Bool
}

extension FilterViewControllerDelegate {
    public func filterViewController(_ viewController: FilterViewController, didDeselectItemAt index: Int) {
    }
    
    public func filterViewController(_ viewController: FilterViewController, shouldSelectItemAt index: Int) -> Bool {
        return true
    }
    
    public func filterViewController(_ viewController: FilterViewController, shouldDeselectItemAt index: Int) -> Bool {
        return true
    }
}

// MARK: - FilterItemCellDelegate

extension FilterViewController: FilterItemCellDelegate {
    open func filterItemCellDidToggle(_ cell: FilterItemCell) {
        guard let indexPath = self.collectionView!.indexPath(for: cell) else {
            return
        }
        
        let index = indexPath.item
        if self.filterData.isSelected(index: index) {
            if self.delegate?.filterViewController(self, shouldDeselectItemAt: index) ?? true {
                self.filterData.toggle(index: index)
                self.delegate?.filterViewController(self, didDeselectItemAt: index)
            }
        } else {
            if self.delegate?.filterViewController(self, shouldSelectItemAt: index) ?? true {
                self.filterData.toggle(index: index)
                self.delegate?.filterViewController(self, didSelectItemAt: index)
            }
        }
    }
}
