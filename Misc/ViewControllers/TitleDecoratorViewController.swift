//
//  TitleDecoratorViewController.swift
//  Misc
//
//  Created by 王义川 on 24/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

public class TitleDecoratorViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    fileprivate var pageViewController: UIPageViewController!
    
    public var titleText: NSAttributedString!
    public var contentViewController: UIViewController!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.attributedText = self.titleText
        self.pageViewController.setViewControllers([self.contentViewController], direction: .forward, animated: false, completion: nil)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case .some("Content"):
            self.pageViewController = segue.destination as! UIPageViewController
        default:
            super.prepare(for: segue, sender: sender)
        }
    }
    
}

extension TitleDecoratorViewController {
    public static func instantiate(title: NSAttributedString, content contentViewController: UIViewController) -> TitleDecoratorViewController {
        let viewController = self.storyboard(name: "Main").instantiateViewController(withIdentifier: "TitleDecorator") as! TitleDecoratorViewController
        viewController.titleText = title
        viewController.contentViewController = contentViewController
        return viewController
    }
}
