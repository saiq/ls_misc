//
//  WebViewController.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

public protocol WebViewControllerDelegate: class {
    func viewDidLoad(webViewController: WebViewController)
    func webViewController(_ viewController: WebViewController, viewWillAppearAnimated animated: Bool)
    func webViewController(_ viewController: WebViewController, viewDidAppearAnimated animated: Bool)
    func webViewController(_ viewController: WebViewController, viewWillDisappearAnimated animated: Bool)
    func webViewController(_ viewController: WebViewController, viewDidDisappearAnimated animated: Bool)
}

open class WebViewController: UIViewController {
    
    public typealias SetupAction = (WebViewController) -> AnyObject?
    
    @IBOutlet public weak var webView: UIWebView?
    @IBOutlet public weak var activityIndicatorView: UIActivityIndicatorView?
    
    @IBInspectable public var urlString: String? {
        get {
            return self.urlRequest?.url?.absoluteString
        }
        set {
            if let url = newValue.flatMap({ URL(string: $0) }) {
                if var urlRequest = self.urlRequest {
                    urlRequest.url = url
                    self.urlRequest = urlRequest
                } else {
                    self.urlRequest = URLRequest(url: url)
                }
            } else {
                self.urlRequest = nil
            }
        }
    }
    public var delegate: WebViewControllerDelegate?
    public var urlRequest: URLRequest?
    public var setupAction: SetupAction?
    private var setupToken: AnyObject?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        if let webView = self.webView, let urlRequest = self.urlRequest {
            webView.scrollView.bounces = false
            webView.delegate = self
            
            self.setupToken = self.setupAction?(self)
            
            webView.loadRequest(urlRequest)
        }
        self.delegate?.viewDidLoad(webViewController: self)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate?.webViewController(self, viewWillAppearAnimated: animated)
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.delegate?.webViewController(self, viewDidAppearAnimated: animated)
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.webViewController(self, viewWillDisappearAnimated: animated)
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.delegate?.webViewController(self, viewDidDisappearAnimated: animated)
    }
}

// MARK: - Instance

extension WebViewController {
    public static func instantiate(url: URL? = nil) -> WebViewController {
        return self.instantiate(urlRequest: url.flatMap({ URLRequest(url: $0) }))
    }
    
    public static func instantiate(urlRequest: URLRequest?) -> WebViewController {
        let viewController = self.storyboard(name: "Main").instantiateViewController(withIdentifier: "WebView") as! WebViewController
        viewController.urlRequest = urlRequest
        return viewController
    }
}

// MARK: - UIWebViewDelegate

extension WebViewController: UIWebViewDelegate {
    open func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicatorView?.startAnimating()
    }
    
    open func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicatorView?.stopAnimating()
    }
    
    open func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicatorView?.stopAnimating()
    }
}
