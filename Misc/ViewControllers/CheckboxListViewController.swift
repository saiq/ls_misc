//
//  CheckboxListViewController.swift
//  Misc
//
//  Created by 王义川 on 20/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

public protocol CheckboxItemProtocol {
    var checkboxText: String { get }
}

extension CheckboxItemProtocol where Self: CustomStringConvertible {
    public var checkboxText: String {
        return self.description
    }
}

extension String: CheckboxItemProtocol {
}

public class CheckboxListViewController: UITableViewController {
    
    public weak var delegate: CheckboxListViewControllerDelegate?
    
    public var items: [CheckboxItemProtocol]! = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    public var selectedIndexes: [Int] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    public var selectedIndex: Int? {
        get {
            return selectedIndexes.first
        }
        set {
            if let index = newValue {
                if 0..<items.count ~= index {
                    self.selectedIndexes = [index]
                }
            } else {
                self.selectedIndexes = []
            }
        }
    }
    public var allowsMultipleSelection: Bool = false {
        didSet {
            self.tableView.allowsMultipleSelection = allowsMultipleSelection
            
            if !allowsMultipleSelection && selectedIndexes.count > 1 {
                selectedIndexes.removeAll()
            }
        }
    }
    public var isDoneButtonHidden: Bool = false
    public var didSelectAction: ((CheckboxListViewController, Int) -> Void)?
    public var didDeselectAction: ((CheckboxListViewController, Int) -> Void)?
    public var doneAction: ((CheckboxListViewController) -> Void)?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsMultipleSelection = self.allowsMultipleSelection
        self.tableView.separatorColor = UIColor(red: CGFloat(0xD9)/CGFloat(0xFF), green: CGFloat(0xD9)/CGFloat(0xFF), blue: CGFloat(0xD9)/CGFloat(0xFF), alpha: 1)
        
        if !isDoneButtonHidden {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        }
    }
    
    @objc private dynamic func done() {
        self.doneAction?(self)
        self.delegate?.done(self)
    }
}

extension CheckboxListViewController {
    public static func instantiate(items: [CheckboxItemProtocol], selectedIndex: Int? = nil) -> CheckboxListViewController {
        let viewController = self.storyboard(name: "Main").instantiateViewController(withIdentifier: "CheckboxList") as! CheckboxListViewController
        viewController.items = items
        viewController.selectedIndex = selectedIndex
        return viewController
    }
}

public protocol CheckboxListViewControllerDelegate: class {
    func checkboxListViewController(_ viewController: CheckboxListViewController, shouldSelect index: Int) -> Bool
    func checkboxListViewController(_ viewController: CheckboxListViewController, didSelect index: Int)
    func checkboxListViewController(_ viewController: CheckboxListViewController, didDeselect index: Int)
    func done(_ viewController: CheckboxListViewController)
}

extension CheckboxListViewControllerDelegate {
    public func checkboxListViewController(_ viewController: CheckboxListViewController, shouldSelect index: Int) -> Bool {
        return true
    }
    
    public func checkboxListViewController(_ viewController: CheckboxListViewController, didSelect index: Int) {
    }
    
    public func checkboxListViewController(_ viewController: CheckboxListViewController, didDeselect index: Int) {
    }
}

// MARK: - UITableViewDataSource

extension CheckboxListViewController {
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckboxCell", for: indexPath) as! CheckboxCell
        let item = self.items[indexPath.item]
        cell.titleLabel.text = item.checkboxText
        cell.checkImageView.isHidden = !self.selectedIndexes.contains(indexPath.item)
        return cell
    }
}

// MARK: - UITableViewDataDelegate

extension CheckboxListViewController {
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if !(self.delegate?.checkboxListViewController(self, shouldSelect: indexPath.item) ?? true) {
            return
        }
        
        if self.allowsMultipleSelection {
            if let index = self.selectedIndexes.index(where: { $0 == indexPath.item }) {
                self.selectedIndexes.remove(at: index)
                
                self.didDeselectAction?(self, indexPath.item)
                self.delegate?.checkboxListViewController(self, didDeselect: indexPath.item)
            } else {
                self.selectedIndexes.append(indexPath.item)
                
                self.didSelectAction?(self, indexPath.item)
                self.delegate?.checkboxListViewController(self, didSelect: indexPath.item)
            }
        } else {
            self.selectedIndexes = [indexPath.item]
            
            self.didSelectAction?(self, indexPath.item)
            self.delegate?.checkboxListViewController(self, didSelect: indexPath.item)
        }
    }
}
