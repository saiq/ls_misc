//
//  ProgressHUD.swift
//  Misc
//
//  Created by 王义川 on 03/05/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

public protocol ProgressHUDProtocol: class {
    func show()
    func dismiss()
}
