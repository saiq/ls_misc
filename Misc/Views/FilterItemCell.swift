//
//  FilterItemCell.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

public class FilterItemCell: UICollectionViewCell {
    @IBOutlet public weak var button: UIButton!
    
    public weak var delegate: FilterItemCellDelegate?
    
    @IBAction public func onButtonTapped() {
        self.delegate?.filterItemCellDidToggle(self)
    }
}

public protocol FilterItemCellDelegate: class {
    func filterItemCellDidToggle(_ cell: FilterItemCell)
}
