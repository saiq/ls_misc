//
//  CheckboxCell.swift
//  Misc
//
//  Created by 王义川 on 20/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

class CheckboxCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
}
