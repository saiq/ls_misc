//
//  InstantiatableViewProtocol.swift
//  Misc
//
//  Created by 王义川 on 18/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

public protocol InstantiatableViewProtocol {
    associatedtype View: UIView
    
    static func instantiate() -> View
}
