//
//  TableViewHeaderFooterView.swift
//  Misc
//
//  Created by 王义川 on 18/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

public class TableViewHeaderFooterView<T: InstantiatableViewProtocol>: UITableViewHeaderFooterView {
    
    public private(set) var bodyView: T.View!
    
    public override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        let view = T.instantiate()
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        self.contentView.addConstraints([
            NSLayoutConstraint(item: view, toItem: self.contentView, attribute: .leading),
            NSLayoutConstraint(item: view, toItem: self.contentView, attribute: .trailing),
            NSLayoutConstraint(item: view, toItem: self.contentView, attribute: .top),
            NSLayoutConstraint(item: view, toItem: self.contentView, attribute: .bottom),
            ])
        self.bodyView = view
    }
}
