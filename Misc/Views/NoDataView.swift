//
//  NoDataView.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

open class NoDataView: UIView {
    
    @IBOutlet public weak var textLabel: UILabel!
    @IBOutlet weak var centerYLayoutConstraint: NSLayoutConstraint!
    
    public var verticalOffset: CGFloat {
        get {
            return self.centerYLayoutConstraint.constant
        }
        set {
            self.centerYLayoutConstraint.constant = newValue
        }
    }
    
    public var action: ((NoDataView) -> Void)?
    
    @IBAction public func onTapped() {
        self.action?(self)
    }
}

extension NoDataView {
    public static var nib: UINib {
        return self.nib(nibName: "NoDataView")
    }
    
    public static func instantiate(withOwner ownerOrNil: Any? = nil, options optionsOrNil: [AnyHashable : Any]? = nil, action: ((NoDataView) -> Void)? = nil) -> NoDataView {
        let view = nib.instantiate(withOwner: ownerOrNil, options: optionsOrNil).first! as! NoDataView
        view.action = action
        return view
    }
}
