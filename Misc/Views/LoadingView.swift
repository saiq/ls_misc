//
//  LoadingView.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

open class LoadingView: UIView {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet public weak var textLabel: UILabel!
    @IBOutlet weak var centerYLayoutConstraint: NSLayoutConstraint!
    
    public var verticalOffset: CGFloat {
        get {
            return self.centerYLayoutConstraint.constant
        }
        set {
            self.centerYLayoutConstraint.constant = newValue
        }
    }
    
    open override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        if newSuperview != nil && !self.activityIndicatorView.isAnimating {
            self.activityIndicatorView.startAnimating()
        } else if newSuperview == nil && self.activityIndicatorView.isAnimating {
            self.activityIndicatorView.stopAnimating()
        }
    }
}

extension LoadingView {
    public static var nib: UINib {
        return self.nib(nibName: "LoadingView")
    }
    
    public static func instantiate(withOwner ownerOrNil: Any? = nil, options optionsOrNil: [AnyHashable : Any]? = nil) -> LoadingView {
        return nib.instantiate(withOwner: ownerOrNil, options: optionsOrNil).first! as! LoadingView
    }
}
