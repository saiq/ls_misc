//
//  NoDataView.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

open class NoDataLogoView: UIView {
    
    @IBOutlet public weak var textLabel: UILabel!
    @IBOutlet public weak var logoImageView: UIImageView!
    @IBOutlet weak var centerYLayoutConstraint: NSLayoutConstraint!
    
    public var verticalOffset: CGFloat {
        get {
            return self.centerYLayoutConstraint.constant
        }
        set {
            self.centerYLayoutConstraint.constant = newValue
        }
    }
    
    public var action: ((NoDataLogoView) -> Void)?
    
    @IBAction public func onTapped() {
        self.action?(self)
    }
}

extension NoDataLogoView {
    public static var nib: UINib {
        return self.nib(nibName: "NoDataLogoView")
    }
    
    public static func instantiate(withOwner ownerOrNil: Any? = nil, options optionsOrNil: [AnyHashable : Any]? = nil, action: ((NoDataLogoView) -> Void)? = nil) -> NoDataLogoView {
        let view = nib.instantiate(withOwner: ownerOrNil, options: optionsOrNil).first! as! NoDataLogoView
        view.action = action
        return view
    }
}
