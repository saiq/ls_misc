//
//  NSAttributedString+Convenience.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

extension NSAttributedString {
    public convenience init(string: String, color: UIColor) {
        self.init(string: string, attributes: [.foregroundColor: color])
    }
    
    public convenience init(string: String, font: UIFont) {
        self.init(string: string, attributes: [.font: font])
    }
    
    public convenience init(string: String, color: UIColor, font: UIFont) {
        self.init(string: string, attributes: [.foregroundColor: color, .font: font])
    }
}
