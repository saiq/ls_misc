//
//  UIViewController+Exit.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

extension UIViewController {
    public func exit(animated: Bool = true) {
        if let navigationController = self.navigationController {
            if let index = navigationController.viewControllers.index(of: self), index > 0 {
                navigationController.popToViewController(navigationController.viewControllers[index - 1], animated: animated)
            } else if navigationController.presentingViewController != nil {
                navigationController.dismiss(animated: animated, completion: nil)
            }
        } else if self.presentingViewController != nil {
            self.dismiss(animated: animated, completion: nil)
        }
    }
}
