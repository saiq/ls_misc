//
//  Module.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

extension UIViewController {
    public static func storyboard(name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle: Bundle(for: self))
    }
}


extension UIView {
    public static func nib(nibName: String) -> UINib {
        return UINib(nibName: nibName, bundle: Bundle(for: self))
    }
}
