//
//  NSLayoutConstraint+Enabled.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

extension NSLayoutConstraint {
    public var isEnabled: Bool {
        get {
            return self.priority.rawValue > UILayoutPriority.defaultHigh.rawValue
        }
        set {
            if newValue {
                self.priority = UILayoutPriority(rawValue: UILayoutPriority.defaultHigh.rawValue + 1)
            } else {
                self.priority = UILayoutPriority(rawValue: UILayoutPriority.defaultHigh.rawValue - 1)
            }
        }
    }
}
