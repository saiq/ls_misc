//
//  NSLayoutConstraint+Convenience.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

extension NSLayoutConstraint {
    public convenience init(item view1: Any, toItem view2: Any?, attribute attr: NSLayoutAttribute, multiplier: CGFloat = 1, constant c: CGFloat = 0) {
        self.init(item: view1, attribute: attr, relatedBy: .equal, toItem: view2, attribute: attr, multiplier: multiplier, constant: c)
    }
    
    public convenience init(item view1: Any, attribute attr1: NSLayoutAttribute, toItem view2: Any?, attribute attr2: NSLayoutAttribute, multiplier: CGFloat = 1, constant c: CGFloat = 0) {
        self.init(item: view1, attribute: attr1, relatedBy: .equal, toItem: view2, attribute: attr2, multiplier: multiplier, constant: c)
    }
}
