//
//  UIViewController+Lifecycle.swift
//  Misc
//
//  Created by 王义川 on 04/09/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

extension UIViewController {
    public enum LifecycleEventType {
        case viewDidLoad
        case viewWillAppear
        case viewDidAppear
        case viewWillDisappear
        case viewDidDisappear
    }
}
