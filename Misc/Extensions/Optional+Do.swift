//
//  Optional+Do.swift
//  Misc
//
//  Created by 王义川 on 13/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

extension Optional {
    public func `do`(_ action: (Wrapped) -> Void) {
        switch self {
        case .some(let wrapped):
            action(wrapped)
        case .none: ()
        }
    }
}
