//
//  ToNumberString.swift
//  Misc
//
//  Created by 王义川 on 18/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

extension Double {
    public func toNumberString() -> String {
        let intValue = Int64(self)
        if self != Double(intValue) {
            return self.description
        } else {
            return intValue.description
        }
    }
}

extension Float {
    public func toNumberString() -> String {
        let intValue = Int(self)
        if self != Float(intValue) {
            return self.description
        } else {
            return intValue.description
        }
    }
}
