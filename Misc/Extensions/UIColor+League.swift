//
//  UIColor+League.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

extension UIColor {
    public struct League {
        private init() {}
        
        public static var colors: [UIColor] {
            return [UIColor(red: CGFloat(0x00)/CGFloat(0xFF), green: CGFloat(0x66)/CGFloat(0xFF), blue: CGFloat(0x66)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x1E)/CGFloat(0xFF), green: CGFloat(0x6E)/CGFloat(0xFF), blue: CGFloat(0xC7)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0xE8)/CGFloat(0xFF), green: CGFloat(0x81)/CGFloat(0xFF), blue: CGFloat(0x1A)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x94)/CGFloat(0xFF), green: CGFloat(0x97)/CGFloat(0xFF), blue: CGFloat(0x20)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x88)/CGFloat(0xFF), green: CGFloat(0x57)/CGFloat(0xFF), blue: CGFloat(0xED)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x1C)/CGFloat(0xFF), green: CGFloat(0xA5)/CGFloat(0xFF), blue: CGFloat(0x86)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0xF4)/CGFloat(0xFF), green: CGFloat(0x6F)/CGFloat(0xFF), blue: CGFloat(0x2C)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x56)/CGFloat(0xFF), green: CGFloat(0xBF)/CGFloat(0xFF), blue: CGFloat(0x0A)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x56)/CGFloat(0xFF), green: CGFloat(0x4F)/CGFloat(0xFF), blue: CGFloat(0xC0)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x99)/CGFloat(0xFF), green: CGFloat(0x67)/CGFloat(0xFF), blue: CGFloat(0x33)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x7A)/CGFloat(0xFF), green: CGFloat(0x76)/CGFloat(0xFF), blue: CGFloat(0x30)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x99)/CGFloat(0xFF), green: CGFloat(0x90)/CGFloat(0xFF), blue: CGFloat(0x12)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0xFF)/CGFloat(0xFF), green: CGFloat(0x66)/CGFloat(0xFF), blue: CGFloat(0x33)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0xCA)/CGFloat(0xFF), green: CGFloat(0x00)/CGFloat(0xFF), blue: CGFloat(0xCA)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x1B)/CGFloat(0xFF), green: CGFloat(0xA5)/CGFloat(0xFF), blue: CGFloat(0x70)/CGFloat(0xFF), alpha: 1),
                    UIColor(red: CGFloat(0x99)/CGFloat(0xFF), green: CGFloat(0x00)/CGFloat(0xFF), blue: CGFloat(0x99)/CGFloat(0xFF), alpha: 1),]
        }
        
        public static func color(for leagueId: Int) -> UIColor {
            return colors[leagueId % colors.count]
        }
    }
}
