//
//  UIImage+Scale.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit

extension UIImage {
    public func scale(to size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        defer {
            UIGraphicsEndImageContext()
        }
        
        self.draw(in: CGRect(origin: CGPoint.zero, size: size))
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    public func scale(maxWidth: CGFloat) -> UIImage {
        return self.scale(maxWidth: maxWidth, maxHeight: nil)
    }
    
    public func scale(maxHeight: CGFloat) -> UIImage {
        return self.scale(maxWidth: nil, maxHeight: maxHeight)
    }
    
    private func scale(maxWidth: CGFloat?, maxHeight: CGFloat?) -> UIImage {
        if (maxWidth == nil || self.size.width <= maxWidth!) && (maxHeight == nil || self.size.height <= maxHeight!) {
            return self.scale(to: self.size)
        }
        
        let widthFactor: CGFloat
        if let maxWidth = maxWidth {
            widthFactor = maxWidth / self.size.width
        } else {
            widthFactor = 1
        }
        
        let heightFactor: CGFloat
        if let maxHeight = maxHeight {
            heightFactor = maxHeight / self.size.height
        } else {
            heightFactor = 1
        }
        
        let scaleFactor = min(widthFactor, heightFactor)
        let scaledWidth = self.size.width * scaleFactor
        let scaledHeight = self.size.height * scaleFactor
        return self.scale(to: CGSize(width: scaledWidth, height: scaledHeight))
    }
}
