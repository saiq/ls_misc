//
//  Dictionary+Equal.swift
//  Misc
//
//  Created by 王义川 on 12/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

public func ==<Key1, Key2, Value: Equatable>(lhs: [Key1: [Key2: Value]], rhs: [Key1: [Key2: Value]]) -> Bool {
    guard lhs.count == rhs.count else {
        return false
    }
    for (key, leftValue) in lhs {
        if let rightValue = rhs[key], rightValue == leftValue {
            continue
        }
        return false
    }
    return true
}
