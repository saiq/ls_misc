//
//  Data+Encrypt.swift
//  Misc
//
//  Created by 钟浩良 on 2017/12/25.
//  Copyright © 2017年 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

extension Data {
    public func xorEncrypt(key: Data) -> Data {
        guard !key.isEmpty else {
            return self
        }
        var encryptedData = Data()
        for (index, bit) in self.enumerated() {
            encryptedData.append(bit ^ key[index % key.count])
        }
        return encryptedData
    }
}
