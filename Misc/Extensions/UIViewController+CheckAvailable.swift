//
//  UIViewController+CheckAvailable.swift
//  Misc
//
//  Created by 王义川 on 08/05/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import UIKit
import AssetsLibrary
import AVFoundation

extension UIViewController {
    public func checkAvailable(sourceType: UIImagePickerControllerSourceType) -> Bool {
        let message: String
        switch sourceType {
        case .camera:
            message = "请开启相机：设置 > 隐私 > 相机"
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .restricted, .denied:
                self.presentPermissionDenied(message: message)
                return false
            case .notDetermined, .authorized: ()
            }
        case .photoLibrary, .savedPhotosAlbum:
            message = "请开启照片：设置 > 隐私 > 照片"
            switch ALAssetsLibrary.authorizationStatus() {
            case .restricted, .denied:
                self.presentPermissionDenied(message: message)
                return false
            case .notDetermined, .authorized: ()
            }
        }
        if !UIImagePickerController.isSourceTypeAvailable(sourceType) {
            self.presentPermissionDenied(message: message)
            return false
        }
        return true
    }
    
    private func presentPermissionDenied(message: String) {
        var viewController: UIViewController = self
        if let presentedViewController = viewController.presentedViewController {
            viewController = presentedViewController
        }
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "设置", style: .default, handler: { _ in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
}
