//
//  Reusable.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

public protocol ReusableViewProtocol {
    associatedtype View
    
    var identifier: String { get }
}

public struct ReusableView<T>: ReusableViewProtocol {
    public typealias View = T
    
    public let identifier: String
    
    public init(_ identifier: String) {
        self.identifier = identifier
    }
}

extension UITableView {
    public func dequeueReusableCell<T: ReusableViewProtocol>(_ reusableView: T, for indexPath: IndexPath) -> T.View where T.View: UITableViewCell {
        return self.dequeueReusableCell(withIdentifier: reusableView.identifier, for: indexPath) as! T.View
    }
    
    public func dequeueReusableHeaderFooterView<T: ReusableViewProtocol>(_ reusableView: T) -> T.View where T.View: UITableViewHeaderFooterView {
        return self.dequeueReusableHeaderFooterView(withIdentifier: reusableView.identifier) as! T.View
    }
    
    public func register<T: ReusableViewProtocol>(_ reusableView: T) where T.View: UITableViewHeaderFooterView {
        self.register(type(of: reusableView.self).View.self, forHeaderFooterViewReuseIdentifier: reusableView.identifier)
    }
}

extension UICollectionView {
    public func dequeueReusableCell<T: ReusableViewProtocol>(_ reusableView: T, for indexPath: IndexPath) -> T.View where T.View: UICollectionViewCell {
        return self.dequeueReusableCell(withReuseIdentifier: reusableView.identifier, for: indexPath) as! T.View
    }
    
    public func dequeueReusableSupplementaryView<T: ReusableViewProtocol>(reusableView: T, kind: String, for indexPath: IndexPath) -> T.View where T.View: UICollectionReusableView {
        return self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reusableView.identifier, for: indexPath) as! T.View
    }
}
