//
//  Refreshable.swift
//  Misc
//
//  Created by 王义川 on 11/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

/// 可刷新的
public protocol Refreshable {
    /// 刷新
    func refresh()
}
