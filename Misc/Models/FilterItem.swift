//
//  FilterItem.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

public protocol FilterItemProtocol {
    var text: String { get }
}

extension String: FilterItemProtocol {
    public var text: String {
        return self
    }
}
