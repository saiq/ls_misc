//
//  FilterData.swift
//  Misc
//
//  Created by 王义川 on 10/04/2017.
//  Copyright © 2017 肇庆市华盈体育文化发展有限公司. All rights reserved.
//

import Foundation

public struct FilterData {
    public let items: [FilterItemProtocol]
    
    private var _selectedIndexes: [Int]?
    public var selectedIndexes: [Int]? {
        get {
            return _selectedIndexes
        }
        set {
            if let newValue = newValue {
                let indexes = newValue.reduce([Int]()) { array, value in
                    var array = array
                    if 0..<items.count ~= value && !array.contains(value) {
                        array.append(value)
                    }
                    return array
                }
                if indexes.count == items.count {
                    _selectedIndexes = nil
                } else {
                    _selectedIndexes = indexes
                }
            } else {
                _selectedIndexes = nil
            }
        }
    }
    public var selectedItems: [FilterItemProtocol]? {
        guard let indexes = self._selectedIndexes else {
            return nil
        }
        return indexes.map { self.items[$0] }
    }
    
    public init(items: [FilterItemProtocol], selectedIndexes: [Int]? = nil) {
        self.items = items
        self.selectedIndexes = selectedIndexes
    }
    
    public mutating func selectAll() {
        self._selectedIndexes = nil
    }
    
    public mutating func unselectAll() {
        self._selectedIndexes = []
    }
    
    public mutating func toggle(index: Int) {
        guard 0..<self.items.count ~= index else {
            return
        }
        
        if var indexes = self.selectedIndexes {
            if let i = indexes.index(of: index) {
                indexes.remove(at: i)
            } else {
                indexes.append(index)
            }
            if indexes.count == self.items.count {
                self._selectedIndexes = nil
            } else {
                self._selectedIndexes = indexes
            }
        } else {
            self._selectedIndexes = self.items.enumerated().flatMap { idx, _ -> Int? in
                return idx == index ? nil : idx
            }
        }
    }
    
    public func isSelected(index: Int) -> Bool {
        guard let selectedIndexes = self._selectedIndexes else {
            return true
        }
        return selectedIndexes.contains(index)
    }
}
